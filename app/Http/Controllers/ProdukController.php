<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Produkdetail;
use Illuminate\Support\Facades\DB;

class ProdukController extends Controller
{
	public function index()
	{
		$produk = DB::table('produk');
		$produk = $produk->get();
		return view('pencarian',['produk' => $produk]);
	}

	public function cari(Request $request)
	{
		$cari = $request-> cari;

		$produk = DB::table('produk')->where('nama_barang','like',"%".$cari."%");
		$produk = $produk->get();
		return view('pencarian',['produk' => $produk]);

	}

}