<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produkdetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_barang',
        'nama_barang',
        'harga',
        'stok',
        'deskripsi',
    ];
}