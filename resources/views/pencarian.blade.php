<html>
<head>
	<title>Halaman Pencarian</title>
</head>
<body>
 
	<p>Menu Pencarian</p>
	<form action="/produk/cari" method="GET">
		<input type="text" name="cari" placeholder="Cari Barang .." value="{{ old('cari') }}">
		<input type="submit" value="CARI">
	</form>
 
	<p>Daftar Barang</p>
	<table>
		<tr>
			<th>Nama Barang</th>
			<th>Harga</th>
			<th>Kategori</th>
		</tr>
		@foreach($produk as $p)
		<tr>
			<td>{{ $p->nama_barang }}</td>
			<td>{{ $p->harga }}</td>
			<td>{{ $p->kategori }}</td>
			<td><a href="{{ url('produk') }}/{{ $p->id_barang }}" class="btn">Detail</a></td>
		</tr>
		@endforeach
	</table>
 
</body>
</html>
